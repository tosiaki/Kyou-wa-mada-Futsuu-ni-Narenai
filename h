[33mcommit 7091a6bec36163cdfe1d8761accab58eb086930d[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Fri May 20 21:33:53 2022 -0400

    Adding chapters 10 and 11.

[33mcommit 273884c27c1fb9b62aa8518581bd29de80828fb1[m[33m ([m[1;31morigin/master[m[33m)[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sat May 14 23:53:34 2022 -0400

    Adding chapter 9.

[33mcommit b26efc3dfc1a25dcdbba2af2c2e660f54f674a0c[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Wed May 11 15:50:55 2022 -0400

    Changing one word.

[33mcommit 5170573c44588fb3bd695a2d76e786e8ecb09cd9[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Wed May 11 15:49:21 2022 -0400

    Adding chapter 8.

[33mcommit be93e073b0dfa2e0df65c73b118bdc8f634e1c95[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Tue May 10 20:00:40 2022 -0400

    Adding chapter 7.

[33mcommit 7b08fbab7cb8e922cfd24a83796e878fdfa16e82[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Mon May 9 18:33:25 2022 -0400

    Adding chapter 6.

[33mcommit c50c3e03fa6a49e2efc7232c0c9e141e64e61080[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sun May 8 21:28:14 2022 -0400

    Adding chapter 5.

[33mcommit ae6b89a54fb3d638b2a329705c8601d66b723b70[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sat May 7 21:30:48 2022 -0400

    Correcting word.

[33mcommit 64ee8b26fe6cd4c370b2db91b4847a1c47e2de6a[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sat May 7 21:30:17 2022 -0400

    Correcting typo.

[33mcommit 7db4cc09ccc2e192be1f6fb032746ad6c295c784[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Fri May 6 19:22:53 2022 -0400

    Adding chapter 4.

[33mcommit 91fed6de0a64d454accb1114314f89f61c5d83bd[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Thu Apr 28 19:14:19 2022 -0400

    Adding chapter 2 and 3.

[33mcommit 82880ec92ff7bbb30b23de132c08dc570683f7bb[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sun Apr 10 17:20:52 2022 -0400

    Fixing typo.

[33mcommit f93c70bfd69b2c53e39963aa0dc44ae17620e237[m
Author: Tosiaki 「」 <tosiaki@example.com>
Date:   Sun Apr 10 17:15:56 2022 -0400

    Initializing document.
